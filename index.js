var hsl = require('hsl-to-hex')
var express = require('express');
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

function isHex(t)
{
    var regex = /(^#?[0-9A-F][0-9A-F][0-9A-F]$)|(^#?[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]$)/i;
    return regex.test(t);
}

function isRGB(t)
{
    var regex = /^rgb\(\s*?([0]|[0-9][0-9]|[0-1][0-9][0-9]|[2][0-5][0-5])\s*?,\s*?([0]|[0-9][0-9]|[0-1][0-9][0-9]|[2][0-5][0-5])\s*?,\s*?([0]|[0-9][0-9]|[0-1][0-9][0-9]|[2][0-5][0-5])\s*?\)$/i;
    return t.match(regex);
}

function isHSL(t){
    var regex = /^hsl\(\s*?([0]|[0-9][0-9]|[0-2][0-9][0-9]|[3][0-5][0-9]|360)\s*?,\s*?([0]|[0-9][0-9]|100)%\s*?,\s*?([0]|[0-9][0-9]|100)%\s*?\)$/i;
    return t.match(regex);
}

function d2h(d) {
    return (+d).toString(16).toLowerCase().length == 1?"0" + (+d).toString(16).toLowerCase():(+d).toString(16).toLowerCase();
}

app.get('/', function (req, res) {
    var color = typeof (req.query.color) === "undefined"?"":req.query.color.trim();
    color = color.replace(/%20/g, " ");
    console.log(isRGB(color));
    if (color.length == 3 && isHex(color)) {
        var ans = "#";
        for (var i=0;i<3;i++) {
            ans += color[i].toLowerCase();
            ans += color[i].toLowerCase();
        }
        console.log(ans);
        res.send(ans);
    }
    else if (color.length == 4 && isHex(color)) {
        var ans = "#";
        for (var i=1;i<4;i++) {
            ans += color[i].toLowerCase();
            ans += color[i].toLowerCase();
        }
        console.log(ans);
        res.send(ans);
    }
    else if (color.length == 6 && isHex(color)) {
        console.log(color.toLowerCase());
        res.send("#" + color.toLowerCase());
    }
    else if (color.length == 7 && isHex(color)) {
        console.log(color.toLowerCase());
        res.send(color.toLowerCase());
    }
    else if (isRGB(color)) {
        var arr = isRGB(color);
        console.log(isRGB(color));
        res.send("#" + d2h(arr[1]) + d2h(arr[2]) + d2h(arr[3]));
    }
    else if (isHSL(color)) {
        var arr = isHSL(color);
        console.log(isHSL(color));
        res.send(hsl(arr[1], arr[2], arr[3]));
    }
    else {
        console.log(color);
        res.send("Invalid color");
    }
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});